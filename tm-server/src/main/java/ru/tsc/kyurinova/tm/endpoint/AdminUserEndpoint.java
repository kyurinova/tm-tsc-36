package ru.tsc.kyurinova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.endpoint.IAdminUserEndpoint;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.api.service.IUserService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminUserEndpoint implements IAdminUserEndpoint {

    private IUserService userService;

    private ISessionService sessionService;

    public AdminUserEndpoint(@NotNull IUserService userService, @NotNull ISessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void addUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @WebParam(name = "entity", partName = "entity")
                    User entity
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.add(entity);
    }

    @Override
    @WebMethod
    public void addAllUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @NotNull
            @WebParam(name = "list", partName = "list")
                    List<User> list
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.addAll(list);
    }

    @Override
    @WebMethod
    public void removeUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @WebParam(name = "entity", partName = "entity")
                    User entity
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.remove(entity);
    }

    @Override
    @WebMethod
    public void clearUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.clear();
    }

    @Override
    @WebMethod
    public @Nullable User removeByIdUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeById(id);
    }

    @Override
    @WebMethod
    public @Nullable User removeByIndexUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeByIndex(index);
    }

    @Override
    @WebMethod
    public @Nullable User removeByLoginUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeByLogin(login);
    }

    @Override
    @WebMethod
    public @NotNull User createUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.create(login, password);
    }

    @Override
    @WebMethod
    public @NotNull User createUserEmail(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password,
            @Nullable
            @WebParam(name = "email", partName = "email")
                    String email
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.create(login, password, email);
    }

    @Override
    @WebMethod
    public @NotNull User createUserRole(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password,
            @Nullable
            @WebParam(name = "role", partName = "role")
                    Role role
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.create(login, password, role);
    }

    @Override
    @WebMethod
    public @Nullable User setPasswordUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "userId", partName = "userId")
                    String userId,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.setPassword(userId, password);
    }

    @Override
    @WebMethod
    public @Nullable User updateUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "userId", partName = "userId")
                    String userId,
            @Nullable
            @WebParam(name = "firstName", partName = "firstName")
                    String firstName,
            @Nullable
            @WebParam(name = "lastName", partName = "lastName")
                    String lastName,
            @Nullable
            @WebParam(name = "middleName", partName = "middleName")
                    String middleName
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    @WebMethod
    public @Nullable User lockUserByLogin(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public @Nullable User unlockUserByLogin(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.unlockUserByLogin(login);
    }

}
