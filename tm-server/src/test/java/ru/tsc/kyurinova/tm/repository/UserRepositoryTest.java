package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.model.User;

public class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final User user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final String userEmail = "userTest@mail.ru";

    public UserRepositoryTest() {
        user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setEmail(userEmail);
    }

    @Before
    public void before() {
        userRepository.add(user);
    }

    @Test
    public void findByUserTest() {
        Assert.assertEquals(user, userRepository.findById(userId));
        Assert.assertEquals(user, userRepository.findByIndex(0));
        Assert.assertEquals(user, userRepository.findByLogin(userLogin));
        Assert.assertEquals(user, userRepository.findByEmail(userEmail));
    }

    @Test
    public void removeUserTest() {
        Assert.assertNotNull(user);
        userRepository.remove(user);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByLoginTest() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(userLogin);
        userRepository.removeByLogin(userLogin);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByIdTest() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(userId);
        userRepository.removeById(userId);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByIdIndex() {
        Assert.assertNotNull(user);
        userRepository.removeByIndex(0);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @After
    public void after() {
        userRepository.clear();
    }

}
