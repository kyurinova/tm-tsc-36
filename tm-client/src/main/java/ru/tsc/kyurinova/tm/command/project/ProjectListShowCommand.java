package ru.tsc.kyurinova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractProjectCommand;
import ru.tsc.kyurinova.tm.endpoint.Session;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.enumerated.Sort;
import ru.tsc.kyurinova.tm.endpoint.Project;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListShowCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @Nullable final List<Project> projects;
        System.out.println("[LIST PROJECTS]");
        if (sort == null || sort.isEmpty())
            projects = serviceLocator.getProjectEndpoint().findAllProject(session);
        else {
            projects = serviceLocator.getProjectEndpoint().findAllProjectSorted(session, sort);
        }
        for (@NotNull final Project project : projects) {
            System.out.println(projects.indexOf(project) + 1 + ". " + project.getId() + ". " + project.getName() + ". " + project.getDescription() + ". " + project.getStatus() + ". " + project.getStartDate());
        }
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
