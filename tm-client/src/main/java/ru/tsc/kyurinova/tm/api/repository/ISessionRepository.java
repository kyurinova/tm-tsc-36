package ru.tsc.kyurinova.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.endpoint.Session;

public interface ISessionRepository {
    @Nullable Session getSession();

    void setSession(@Nullable Session session);
}
