package ru.tsc.kyurinova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.endpoint.Session;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "update-user";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "update user's profile...";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME: ");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME: ");
        @NotNull final String middleName = TerminalUtil.nextLine();
        serviceLocator.getAdminUserEndpoint().updateUser(session, session.getUserId(), firstName, lastName, middleName);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
