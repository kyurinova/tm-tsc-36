package ru.tsc.kyurinova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.endpoint.Session;

public class DataJsonSaveJaxBCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-jaxb-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to json";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON JAXB SAVE]");
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminDataEndpoint().dataJsonSaveJaxB(session);
    }

}
