package ru.tsc.kyurinova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.endpoint.Session;
import ru.tsc.kyurinova.tm.enumerated.Role;

public class DataBinSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-save-bin";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save binary data";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminDataEndpoint().dataBinarySave(session);
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
