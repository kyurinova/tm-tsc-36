package ru.tsc.kyurinova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.endpoint.Session;

public final class BackupSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "backup-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save backup to XML";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminDataEndpoint().dataBackupSave(session);
    }

}
