package ru.tsc.kyurinova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kyurinova.tm.marker.SoapCategory;

public class ProjectTaskEndpointTest {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint;

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @NotNull
    private final String taskName = "taskTest";

    @Nullable
    private String taskId;

    @NotNull
    private Task task;

    @NotNull
    private Project project;

    @Nullable
    private String projectId;

    @NotNull
    private final String projectName = "project";

    @NotNull
    private Session session;

    public ProjectTaskEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final TaskEndpointService taskEndpointService = new TaskEndpointService();
        taskEndpoint = taskEndpointService.getTaskEndpointPort();
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        projectEndpoint = projectEndpointService.getProjectEndpointPort();
        @NotNull final ProjectTaskEndpointService projectTaskEndpointService = new ProjectTaskEndpointService();
        projectTaskEndpoint = projectTaskEndpointService.getProjectTaskEndpointPort();

    }

    @Before
    public void before() {
        session = sessionEndpoint.openSession("test", "test");
        taskEndpoint.createTaskDescr(session, taskName, "taskDescription");
        taskId = taskEndpoint.findByNameTask(session, taskName).getId();
        projectEndpoint.createProjectDescr(session, projectName, "projectDescription");
        projectId = projectEndpoint.findByNameProject(session, projectName).getId();
    }

    @Test
    @Category(SoapCategory.class)
    public void findTaskByProjectIdTest() {
        projectTaskEndpoint.bindTaskById(session, projectId, taskId);
        taskEndpoint.createTaskDescr(session, "task2", "task2");
        @NotNull final String newTaskId = taskEndpoint.findByNameTask(session, "task2").getId();
        projectTaskEndpoint.bindTaskById(session, projectId, newTaskId);
        Assert.assertEquals(2, projectTaskEndpoint.findAllTaskByProjectId(session, projectId).size());
        taskEndpoint.removeByIdTask(session, newTaskId);
    }

    @Test
    @Category(SoapCategory.class)
    public void bindTaskByIdTest() {
        projectTaskEndpoint.bindTaskById(session, projectId, taskId);
        task = taskEndpoint.findByIdTask(session, taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(projectId, task.getProjectId());
    }

    @Test
    @Category(SoapCategory.class)
    public void unbindTaskByIdTest() {
        projectTaskEndpoint.bindTaskById(session, projectId, taskId);
        task = taskEndpoint.findByIdTask(session, taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(projectId, task.getProjectId());
        projectTaskEndpoint.unbindTaskById(session, projectId, taskId);
        Assert.assertNull(taskEndpoint.findByIdTask(session, taskId).getProjectId());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIdTest() {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        projectTaskEndpoint.bindTaskById(session, projectId, taskId);
        projectTaskEndpoint.removeById(session, projectId);
        projectId = null;
        Assert.assertTrue(projectEndpoint.findAllProject(session).isEmpty());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeAllTaskByProjectIdTest() {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        projectTaskEndpoint.bindTaskById(session, projectId, taskId);
        projectTaskEndpoint.removeAllTaskByProjectId(session, projectId);
        Assert.assertTrue(projectTaskEndpoint.findAllTaskByProjectId(session, projectId).isEmpty());
    }

    @After
    public void after() {
        if (projectId != null) projectTaskEndpoint.removeById(session, projectId);
        sessionEndpoint.closeSession(session);
    }

}
